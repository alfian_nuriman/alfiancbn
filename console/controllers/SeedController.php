<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\models\Member;
use common\models\User;


class SeedController extends Controller
{
	public function actionRun()
	{
		$model = new Member();
		$user = new User();
		$api = Yii::$app->helper->getDataApi('https://www.w3schools.com/angular/customers.php', 'records');

		if (!empty($api)) {

			foreach ($api as $data) {
				$model->setIsNewRecord(true);
				$user->setIsNewRecord(true);
				$model->id = null;
				$user->id = null;

				// Set data for member model

				$model->name 	= $data['Name'];
				$model->city 	= $data['City'];
				$model->country = $data['Country'];
				$model->income 	= 5000000; // Set default value

				// set data for user model

				$user->username = Yii::$app->helper->generatePermalink($data['Name']);
				$user->setPassword($user->username);
            	$user->generateAuthKey();
            	$user->email = $user->username . '@' . 'local.dev';

            	// create db transaction
            	$transaction = $user->getDb()->beginTransaction();

            	if ($model->save(false)) {
            		$user->id_member = $model->id;
            		$user->save(false);

            		$transaction->commit();

            		echo "SUCCESS\n";
            	} else {
            		$transaction->rollBack();

            		echo "FAILED\n";
            	}
			}

		} else {
			echo "Data API not found";
		}
	}
}