<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\KreditSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kredits';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kredit-index">
    <div class="panel panel-default">
        <div class="panel-body">
            <?php Pjax::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'attribute' => 'id_owner',
                            'label' => 'Pengaju Kredit',
                            'value' => 'user.member.name'
                        ],

                        [
                            'attribute' => 'id_kendaraan',
                            'label' => 'Jenis Kendaraan',
                            'value' => 'kendaraan.nama'
                        ],

                        [
                            'attribute' => 'harga',
                            'label' => 'Cicilan per-Bulan',
                            'format' => 'raw',
                            'value' => function($model) { return \Yii::$app->helper->encodeRupiah($model->harga); }
                        ],

                        [
                            'attribute' => 'jangka_cicilan',
                            'label' => 'Jangka Cicilan',
                            'format' => 'raw',
                            'value' => function($model) {return $model->jangka_cicilan . ' Bulan';}
                        ],

                        [
                            'attribute' => 'id_status',
                            'label' => 'Status Kredit',
                            'value' => 'kreditStatus.name'
                        ],

                        [
                            'class'     => 'yii\grid\ActionColumn',
                            'template'  => '{approve}{cancel}',
                            'buttons'   => [

                                'approve' => function($url, $model) {
                                    return Html::a(
                                            '<span class="label label-success"><span class="fa fa-lg fa-ban"></span> APPROVE </span>',
                                            $url,
                                            [
                                                'title' => Yii::t('yii', 'Banned'),
                                                'data-confirm' => Yii::t('yii', 'Banned Admin ?'),
                                                'data-method' => 'post',
                                            ]
                                        );
                                },

                                'cancel' => function($url, $model) {
                                    return Html::a(
                                            '<span class="label label-danger"><span class="fa fa-lg fa-ban"></span> CANCEL </span>',
                                            $url,
                                            [
                                                'title' => Yii::t('yii', 'Banned'),
                                                'data-confirm' => Yii::t('yii', 'Banned Admin ?'),
                                                'data-method' => 'post',
                                            ]
                                        );
                                },
                                
                                // 'view' => function($url, $model) {
                                //     return Html::a(
                                //             '<span class="label label-success"><span class="fa fa-folder-open"></span></span>',
                                //             $url
                                //         );
                                // },
                            ],
                        ],
                    ],
                ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>