<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Kredit */

$this->title = 'Update Kredit: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Kredits', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kredit-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
