<?php

namespace backend\controllers;

use Yii;
use common\models\Kredit;
use common\models\KreditSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * KreditController implements the CRUD actions for Kredit model.
 */
class KreditController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->id_priviledges < 4;
                        },
                    ],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete'    => ['POST'],
                    'approve'   => ['POST'],
                    'cancel'    => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Kredit models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KreditSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Kredit model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionApprove($id)
    {
        $model = $this->findModel($id);
        $status = $model->id_status + 1;

        $model->id_status = $status;

        if ($model->id_status == 4) {

            $model->sendEmail([
                'status' => 'DITERIMA',
                'email' => $model->user->email
            ]);

            $model->save(false);

            return $this->redirect(['index']);
        } else {
            $model->save(false);

            return $this->redirect(['index']);
        }
    }

    public function actionCancel($id)
    {
        $model = $this->findModel($id);

        if ($model->id_status == 3) {
            $model->sendEmail([
                'status' => 'DITOLAK',
                'email' => $model->user->email
            ]);
        }

        $model->id_status = 5;
        $model->save(false);

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing Kredit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Kredit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Kredit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Kredit::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
