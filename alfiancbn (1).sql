-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 09, 2017 at 09:23 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alfiancbn`
--

-- --------------------------------------------------------

--
-- Table structure for table `kendaraan`
--

CREATE TABLE IF NOT EXISTS `kendaraan` (
  `id` int(11) NOT NULL,
  `nama` text NOT NULL,
  `harga` decimal(10,0) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kendaraan`
--

INSERT INTO `kendaraan` (`id`, `nama`, `harga`) VALUES
(1, 'Toyota Avanza', '120000000'),
(2, 'Toyota Kijang Innova', '150000000'),
(3, 'Toyota Agya', '170000000'),
(4, 'Daihatsu Xenia', '200000000'),
(5, 'Honda HR-V', '250000000');

-- --------------------------------------------------------

--
-- Table structure for table `kredit`
--

CREATE TABLE IF NOT EXISTS `kredit` (
  `id` int(11) NOT NULL,
  `id_owner` int(11) NOT NULL,
  `id_kendaraan` int(11) NOT NULL,
  `harga` decimal(10,0) NOT NULL,
  `jangka_cicilan` int(11) NOT NULL,
  `id_status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kredit`
--

INSERT INTO `kredit` (`id`, `id_owner`, `id_kendaraan`, `harga`, `jangka_cicilan`, `id_status`) VALUES
(1, 1, 1, '10000000', 12, 4);

-- --------------------------------------------------------

--
-- Table structure for table `kredit_status`
--

CREATE TABLE IF NOT EXISTS `kredit_status` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kredit_status`
--

INSERT INTO `kredit_status` (`id`, `name`) VALUES
(1, 'Menunggu Konfirmasi'),
(2, 'Tahap Persetujuan 1'),
(3, 'Tahap Persetujuan 2'),
(4, 'Selesai'),
(5, 'Ditolak');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `city` text NOT NULL,
  `country` varchar(255) NOT NULL,
  `income` decimal(10,0) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `name`, `city`, `country`, `income`) VALUES
(1, 'Comércio Mineiro', 'São Paulo', 'Brazil', '5000000'),
(2, 'Alfreds Futterkiste', 'Berlin', 'Germany', '5000000'),
(3, 'Alfreds Futterkiste', 'Berlin', 'Germany', '5000000'),
(4, 'Ana Trujillo Emparedados y helados', 'México D.F.', 'Mexico', '5000000'),
(5, 'Antonio Moreno Taquería', 'México D.F.', 'Mexico', '5000000'),
(6, 'Around the Horn', 'London', 'UK', '5000000'),
(7, 'B''s Beverages', 'London', 'UK', '5000000'),
(8, 'Berglunds snabbköp', 'Luleå', 'Sweden', '5000000'),
(9, 'Blauer See Delikatessen', 'Mannheim', 'Germany', '5000000'),
(10, 'Blondel père et fils', 'Strasbourg', 'France', '5000000'),
(11, 'Bólido Comidas preparadas', 'Madrid', 'Spain', '5000000'),
(12, 'Bon app''', 'Marseille', 'France', '5000000'),
(13, 'Bottom-Dollar Marketse', 'Tsawassen', 'Canada', '5000000'),
(14, 'Cactus Comidas para llevar', 'Buenos Aires', 'Argentina', '5000000'),
(15, 'Centro comercial Moctezuma', 'México D.F.', 'Mexico', '5000000'),
(16, 'Chop-suey Chinese', 'Bern', 'Switzerland', '5000000'),
(17, 'Comércio Mineiro', 'São Paulo', 'Brazil', '5000000');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1488982750),
('m130524_201442_init', 1488982753);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `id_member` int(11) unsigned NOT NULL,
  `id_priviledges` int(5) unsigned NOT NULL DEFAULT '4',
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `id_member`, `id_priviledges`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 4, '090320170413comrcio-mineiro', 'I0G9Gwdy1KjtnVpsx0aJUlc6Fvx3OzQt', '$2y$13$dQLb3.ao0eULBsHE7HEuUOginpd4FrtqKZR/i3gSGt2LvZN8qCu.e', NULL, 'alfian4299@gmail.com', 10, 1489029184, 1489029197),
(2, 2, 4, '090320170415alfreds-futterkiste', 'GzbwX7FF3WT-Ko22tSC56IhrAfY_Lp3M', '$2y$13$qSjf.XBq3vf3.UyoI0sjYOTWR3mtqyHhctufhkiFAf7bP6e.W1Fom', NULL, '090320170415alfreds-futterkiste@local.dev', 10, 1489029302, 1489029302),
(3, 3, 4, '090320170416alfreds-futterkiste', 'WOziaiePlzU57nh8CvWBSKMctJZ__R9p', '$2y$13$BR4ZzTDD0yclxAD0yzeLAe2QX7aO73TyydZp10YhF/dU588h9ld0q', NULL, '090320170416alfreds-futterkiste@local.dev', 10, 1489029373, 1489029373),
(4, 4, 4, '090320170416ana-trujillo-emparedados-y-helados', 'VBqL5Zv-Cb0cLT_f8NvjMuNI9t_dUj8G', '$2y$13$JTagqd5fOkLa34F2tVkzlO3GGnH1HZ810QRZLu.uRU8HlzMCLJq9i', NULL, '090320170416ana-trujillo-emparedados-y-helados@local.dev', 10, 1489029374, 1489029374),
(5, 5, 4, '090320170416antonio-moreno-taquera', 'K3VukS7hr6aPyl5SCyoVKSPWvcykXYQV', '$2y$13$gacqBywTVOQgHffru3kFq.ub.dgVij4ZxDiH/sv19lAbJ9sVYlMWa', NULL, '090320170416antonio-moreno-taquera@local.dev', 10, 1489029375, 1489029375),
(6, 6, 4, '090320170416around-the-horn', '4s1T9TcdLMlhy_QCoagEHrz92q8fM-3j', '$2y$13$UgmPgUA0eLXpdd6q0Ei21eTHz26f5rV2jXUCFK9.UUxoqjvACGwES', NULL, '090320170416around-the-horn@local.dev', 10, 1489029376, 1489029376),
(7, 7, 4, '090320170416bs-beverages', 'ZlwP3_OBXjZwOjxfXdiJ5kS3LAt2MoYT', '$2y$13$neFS9dF4EetlVn6.ZpB2TeZvd/NyAjyC52yRp4t48FGMNfyNK1Kf6', NULL, '090320170416bs-beverages@local.dev', 10, 1489029377, 1489029377),
(8, 8, 4, '090320170416berglunds-snabbkp', 'MWxGyxQG6Jv9fJF6MyLm1KkQ0kTZZIY5', '$2y$13$YCz9uncQW7j0gLHPeBb5jurqrmNchrAIgRWFDOHl6nenn9Se4Kn7S', NULL, '090320170416berglunds-snabbkp@local.dev', 10, 1489029378, 1489029378),
(9, 9, 4, '090320170416blauer-see-delikatessen', 'we6WK16JV2zVuBGcyWERU7JoeBhWsoJJ', '$2y$13$8ikdLNp2LfcADjIYs8Tzn.WVVSQF6ublh4qSLdP59/sy0pN8Ey3gu', NULL, '090320170416blauer-see-delikatessen@local.dev', 10, 1489029379, 1489029379),
(10, 10, 4, '090320170416blondel-pre-et-fils', 'a5no1egXBFi1--QeZqOY8WSdJPdaCRGE', '$2y$13$n9y1gE6O44X5evn2M3Hh/ettKvAIuhONbqQbr9FspVZCYkV2hgii.', NULL, '090320170416blondel-pre-et-fils@local.dev', 10, 1489029379, 1489029379),
(11, 11, 4, '090320170416blido-comidas-preparadas', '4mc9_ILkmx-lnIcdMGNaY16DPqMY9RLn', '$2y$13$pjsKzY87V0b0t0npv9sJmeBZfHQedK/VBI2h1idyl1Dq7o834hctK', NULL, '090320170416blido-comidas-preparadas@local.dev', 10, 1489029380, 1489029380),
(12, 12, 4, '090320170416bon-app', 'sSnNCXyHyfxGWzoU3GWJyzQI9ZHtmBua', '$2y$13$nzs/cgIP/dHcIrXJS06.mOpRwStmZ0s23Q6Cns7FK2xjcYHTv35eK', NULL, '090320170416bon-app@local.dev', 10, 1489029381, 1489029381),
(13, 13, 4, '090320170416bottom-dollar-marketse', 'SaVBw5iQh2Fb7pKm9iJLf2fREQpY875C', '$2y$13$hlSdayDXvaQlGD6BJDsFB.RQQWfhNwMbzPZx1/w4yQDbwD.0HE3QW', NULL, '090320170416bottom-dollar-marketse@local.dev', 10, 1489029382, 1489029382),
(14, 14, 4, '090320170416cactus-comidas-para-llevar', 'WqQLQZzyZLWMkMcI80xFMM5vK2HkiUT6', '$2y$13$GwwR.zvZ4jZjIGctVctqi.F3g4xGNvVoZqs0IDsxHpjYfP21IYT0i', NULL, '090320170416cactus-comidas-para-llevar@local.dev', 10, 1489029383, 1489029383),
(15, 15, 4, '090320170416centro-comercial-moctezuma', 'esxW4J4IwvdijeLLp0KKaEYlHOzaGQEb', '$2y$13$RLJrJkRk/doTBViFD2RvM.gMhHEC.2E1dNLCGJUFEiBbb8PGDkstS', NULL, '090320170416centro-comercial-moctezuma@local.dev', 10, 1489029384, 1489029384),
(16, 16, 4, '090320170416chop-suey-chinese', '8vcq39bil0x5Z62fXVDxM9JJS7jgarVI', '$2y$13$oPczuCHoKRyXw6UejcMSSOhOgAmMYM8k2xQLE4NbiD8fAgcspI1ZS', NULL, '090320170416chop-suey-chinese@local.dev', 10, 1489029385, 1489029385),
(17, 17, 4, '090320170416comrcio-mineiro', 'NMNfJAyS0oSEmRrbqpiSMT8-am4Vd5Wi', '$2y$13$IDD.wzKQV3W7L.jqfMz63.B6WuaMygoe3TefAE4W1GmJ03ZeQs/Xa', NULL, '090320170416comrcio-mineiro@local.dev', 10, 1489029386, 1489029386),
(18, 0, 2, 'petugassatu', '2fwoL8Pf5XbR6kqyAACU1CWA1AGf0eyn', '$2y$13$qThcqgnxXKPMNq3y39irAupjaC2W1rLe9K95tS3YY2uSzPkDVsOKC', NULL, 'petugassatu@local.dev', 10, 1489045127, 1489045127),
(19, 19, 3, 'petugasdua', '-Ghia3S34NSlJGSLt16Zkb-cryTkmd6B', '$2y$13$Vu7W2FO8LWgB.00E3q2JT.WW1ASPanCugUXyqwsXEgQE1AF9675l6', NULL, 'petugasdua@local.dev', 10, 1489045210, 1489045210),
(21, 20, 1, 'manager', '2eXoQMef4bNp1dabdF-VW-1bc4ZI-rCR', '$2y$13$0eh8h5ezNnkV5lruOkP4VOa3LatDNGWeG5uPsQQhCDz7IFkZZNYEK', NULL, 'manager@local.dev', 10, 1489045296, 1489045296);

-- --------------------------------------------------------

--
-- Table structure for table `user_priviledges`
--

CREATE TABLE IF NOT EXISTS `user_priviledges` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_priviledges`
--

INSERT INTO `user_priviledges` (`id`, `name`) VALUES
(1, 'Manager'),
(2, 'Petugas 1'),
(3, 'Petugas 2'),
(4, 'Member');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kendaraan`
--
ALTER TABLE `kendaraan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kredit`
--
ALTER TABLE `kredit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kredit_status`
--
ALTER TABLE `kredit_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `id_member` (`id_member`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`),
  ADD KEY `id_member_2` (`id_member`);

--
-- Indexes for table `user_priviledges`
--
ALTER TABLE `user_priviledges`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kendaraan`
--
ALTER TABLE `kendaraan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `kredit`
--
ALTER TABLE `kredit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `kredit_status`
--
ALTER TABLE `kredit_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `user_priviledges`
--
ALTER TABLE `user_priviledges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
