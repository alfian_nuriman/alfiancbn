<?php

/**
 * Helper library
 * Version : 1.0
 * Author : Alfian Nur Iman [alfian.programmer@gmail.com]
 */

namespace common\components;

use yii\base\Component;

class AnHelper extends Component {

	function generateCode($adder)
	{
		date_default_timezone_set('Asia/Jakarta');
		$date = date('dmYHis');
		$code = $adder . '-' .  $date . '-';

		return $code;
	}

	function getLocalDate($format = 'Y-m-d')
	{
		date_default_timezone_set('Asia/Jakarta');
		$data = date($format);

		return $data;
	}

	function generatePermalink($content)
	{
		$separate = '-';

		$content = strtolower($content);

		$content = preg_replace("/[\/\.]/", ' ', $content);
		$content = preg_replace("/[^a-z0-9_\s-]/", '', $content);

		$content = preg_replace("/[\s-]+/", ' ', $content);
		$content = preg_replace("/[\s_]/", $separate, $content);

		$content = substr($content, 0, 255);

		return date('dmYHi') . $content;
	}

	function deleteLocalFile($file_name)
	{
		$delete = unlink($file_name);

		if ($delete) {
			return true;
		} else {
			return false;
		}
	}

	function cutString($string, $limit)
	{
		$string_cut = substr($string, 0, $limit);

		return $string_cut;
	}

	function generateUniqueCode()
	{
		return md5(date('dmYHis') . uniqid(rand(), true));
	}

	function generateMemberCode($adder = 'WRN')
	{
		date_default_timezone_set('Asia/Jakarta');

		$res = $adder . date('dmYHis');

		return $res;
	}

	function encodeRupiah($number = 0)
	{
		$rupiah = number_format($number, 2, ',', '.');

		return $rupiah;
	}

	function getOneString($string)
	{
		$str_count = count(explode(',', $string));
		$count_process = $str_count - 1;

		$arr_res = explode(',', $string, '-' . $count_process);

		$res = implode(',', $arr_res);

		return $res;
	}

	function getDataApi($url, $data_record = null)
	{
		$api = file_get_contents($url);
		$json = json_decode($api, true);
		$data = $json[$data_record];

		return $data;
	}
}