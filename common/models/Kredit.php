<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kredit".
 *
 * @property integer $id
 * @property integer $id_owner
 * @property string $jenis_kendaraan
 * @property string $harga
 * @property integer $jangka_cicilan
 * @property integer $id_status
 */
class Kredit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kredit';
    }

    /**
     * @inheritdoc
     */

    public $jangka_waktu = [

        12 => 12,
        24 => 24,
        36 => 36

    ];

    public $agree;

    public function rules()
    {
        return [
            [['id_kendaraan', 'jangka_cicilan'], 'required'],
            [['id_owner', 'id_kendaraan', 'jangka_cicilan', 'id_status'], 'integer'],
            [['harga'], 'number'],
            [['agree'], 'required', 'requiredValue' => 1, 'message' => 'anda harus menyetujui terlebih dahulu'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_owner' => 'Id Owner',
            'id_kendaraan' => 'Id Kendaraan',
            'harga' => 'Harga',
            'jangka_cicilan' => 'Jangka Cicilan',
            'id_status' => 'Id Status',
            'jangka_waktu' => 'Jangka Waktu',
            'agree' => 'Agree',
        ];
    }

    public function sendEmail($confirm = null)
    {
        return Yii::$app->mailer
            ->compose(
                ['html' => 'userVerification-html', 'text' => 'userVerification-text'],
                ['status' => $confirm['status']]
            )
            ->setTo($confirm['email'])
            ->setFrom([Yii::$app->params['verifyEmail'] => 'ALFIAN'])
            ->setSubject('Pemberitahuan Pengajuan Kredit')
            ->send();
    }

    public function getKendaraan()
    {
        return $this->hasOne(Kendaraan::className(), ['id' => 'id_kendaraan']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id_member' => 'id_owner']);
    }

    public function getKreditStatus()
    {
        return $this->hasOne(KreditStatus::className(), ['id' => 'id_status']);
    }
}
