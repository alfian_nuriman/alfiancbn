<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Kredit;

/**
 * KreditSearch represents the model behind the search form about `common\models\Kredit`.
 */
class KreditSearch extends Kredit
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_owner', 'id_kendaraan', 'jangka_cicilan', 'id_status'], 'integer'],
            [['harga'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query;
        $user = Yii::$app->user->identity->id_priviledges;

        if ($user == 1) {
            $query = Kredit::find()->where(['id_status' => 3]);
        } else if($user == 2) {
            $query = Kredit::find()->where(['id_status' => 1]);
        } else if($user == 3) {
            $query = Kredit::find()->where(['id_status' => 2]);
        } else {
            $query = Kredit::find()->where(['id_status' => 0]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_owner' => $this->id_owner,
            'id_kendaraan' => $this->id_kendaraan,
            'harga' => $this->harga,
            'jangka_cicilan' => $this->jangka_cicilan,
            'id_status' => $this->id_status,
        ]);

        return $dataProvider;
    }
}
