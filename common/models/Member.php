<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "member".
 *
 * @property integer $id
 * @property string $name
 * @property string $city
 * @property string $country
 * @property string $income
 */
class Member extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'member';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'city', 'country', 'income'], 'required'],
            [['city'], 'string'],
            [['income'], 'number'],
            [['name', 'country'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'city' => 'City',
            'country' => 'Country',
            'income' => 'Income',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id_member' => 'id']);
    }
}
