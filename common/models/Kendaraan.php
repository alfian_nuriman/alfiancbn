<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kendaraan".
 *
 * @property integer $id
 * @property string $nama
 * @property string $harga
 */
class Kendaraan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kendaraan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'harga'], 'required'],
            [['nama'], 'string'],
            [['harga'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'harga' => 'Harga',
        ];
    }

    public function getKredit()
    {
        return $this->hasOne(Kredit::className(), ['id_kendaraan' => 'id']);
    }
}
