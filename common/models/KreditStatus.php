<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kredit_status".
 *
 * @property integer $id
 * @property string $name
 */
class KreditStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kredit_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    public function getKredit()
    {
        return $this->hasOne(Kredit::className(), ['id_status' => 'id']);
    }
}
