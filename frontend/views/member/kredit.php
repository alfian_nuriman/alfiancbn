<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\Kendaraan;

/* @var $this yii\web\View */
/* @var $model common\models\Kredit */
/* @var $form ActiveForm */
?>
<div class="member-kredit">
	<div class="container">
		<div class="row">
			<div class="col-xs-4 col-xs-offset-4">
				<?php $form = ActiveForm::begin(); ?>

			        <?=
			            $form->field($model, 'id_kendaraan') 
			             ->dropdownList(
			                 ArrayHelper::map(Kendaraan::find()->all(), 'id', 'nama'),
			                 ['prompt' => 'Pilih kendaraan']
			             )->label(
			                 'Kendaraan',
			                 ['class' => 'label-class']
			             )
			        ?>

			        <?=
			            $form->field($model, 'jangka_cicilan') 
			             ->dropdownList(
			                 $model->jangka_waktu,
			                 ['prompt' => 'Pilih jangka waktu cicilan']
			             )->label(
			                 'Lama Cicilan',
			                 ['class' => 'label-class']
			             )
			        ?>

			        <?=
				    	$form->field($model, 'agree')
				    		->checkbox([
				    			'checked' => false,
				    			'label' => 'Dengan ini saya menyetujui semua syarat dan ketentuan'
				    		]);
				    ?>
			    
			        <div class="form-group">
			            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
			        </div>
			    <?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>

</div><!-- member-kredit -->