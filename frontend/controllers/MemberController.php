<?php

namespace frontend\controllers;

use Yii;
use yii\filters\AccessControl;
use common\models\Kredit;

class MemberController extends \yii\web\Controller
{
	public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'kredit'],
                'rules' => [
                    [
                        'actions' => ['index', 'kredit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionKredit()
    {
    	$model = new Kredit();

    	if ($model->load(Yii::$app->request->post()) && $model->validate()) {
    		
    		$model->id_owner = Yii::$app->user->identity->id_member;
    		$model->id_status = 1;

    		// Count harga cicilan per bulan

    		$cicilan = $model->kendaraan->harga / $model->jangka_cicilan;

    		$model->harga = $cicilan;

    		if ($model->save()) {

    			Yii::$app->getSession()->setFlash('success', 'Silahkan Menunggu KOnfirmasi');

    			return $this->redirect(['index']);
    			
    		} else {
    			return $this->render('kredit', [
	    			'model' => $model
	    		]);
    		}

    	} else {
    		return $this->render('kredit', [
    			'model' => $model
    		]);
    	}
    }

}
